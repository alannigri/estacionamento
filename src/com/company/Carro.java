package com.company;

import java.util.HashMap;

public class Carro {
    private String placa;
    private int CPF;

    public String getPlaca() {
        return placa;
    }

    public int getCPF() {
        return CPF;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public void setCPF(int CPF) {
        this.CPF = CPF;
    }

    public Carro(String placa, int CPF) {
        this.placa = placa;
        this.CPF = CPF;
    }

    @Override
    public String toString() {
        return "Carro{" +
                "placa='" + placa + '\'' +
                ", CPF=" + CPF +
                '}';
    }
}