package com.company;

import java.sql.SQLOutput;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Scanner;

public interface IO {

    static String placa(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Placa do carro:");
        String placa = scanner.next();
        return placa;
    }

    static int CPF(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("CPF do motorista:");
        int CPF = scanner.nextInt();
        return CPF;
    }

    static int menu(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("1 para entrada de veiculo");
        System.out.println("2 para saida de veiculo");
        System.out.println("3 para listar vagas");
        System.out.println("4 para sair");

        int CPF = scanner.nextInt();
        return CPF;
    }

    static void recibo(Carro carro, LocalTime horaEntrada, LocalTime horaSaida){
        LocalDate dataHoje = LocalDate.now();
        System.out.println("************ RECIBO ************");
        System.out.println(carro);
        System.out.println("Horario de entrada: " + horaEntrada);
        System.out.println("Horario de saída: " + horaSaida);
        System.out.println(dataHoje);
        System.out.println("********************************");
    }
}
