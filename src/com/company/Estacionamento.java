package com.company;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Date;
import java.util.HashMap;

public class Estacionamento {
    HashMap<String, Integer> veiculo = new HashMap<>();
    HashMap<Carro, String> carroVaga = new HashMap<>();
    HashMap<Carro, LocalTime> carroHoraEntrada = new HashMap<>();

    public boolean entradaVeiculo(Carro carro) {
        String vagaLivre = buscaVagaLivre();
        if (vagaLivre.equals("null")) {
            System.out.println("Estacionamento lotado!");
            return false;
        }
        veiculo.put(carro.getPlaca(), carro.getCPF());
        carroVaga.put(carro, vagaLivre);
        LocalTime data = LocalTime.now();
        carroHoraEntrada.put(carro, data);
//        System.out.println(carroHoraEntrada);
        return true;
    }

    public boolean saidaVeiculo(String placa) {
        int chave = buscaCarro(placa);
        if (chave == 999) {
            System.out.println("Carro não encontrado");
            return false;
        }
        LocalTime data = LocalTime.now();

        veiculo.remove(chave);
        for (Carro key : carroVaga.keySet()) {
            if (key.getPlaca().equals(placa)) {
                IO.recibo(key, carroHoraEntrada.get(key), data.now());
//                System.out.println(key + " entrou as " + carroHoraEntrada.get(key) + " e está saindo as " +
//                        data.now());
                liberaVaga(carroVaga.get(key));
                carroVaga.remove(key);
                carroHoraEntrada.remove(key);
                return true;
            }
        }
//        for (Carro key : carroHoraEntrada.keySet()) {
//            if (key.getPlaca().equals(placa)) {
//                liberaVaga(carroVaga.get(key));
//                carroVaga.remove(key);
//                return true;
//            }
//        }
        return false;
    }

    public String buscaVagaLivre() {
        for (Vagas vaga : Vagas.values()) {
            if (vaga.getStatus() == 0) {
                String vagaLivre = vaga.toString();
                vaga.setStatus(1);
                System.out.println("Vaga " + vagaLivre + " está livre! Dirija-se à ela!");
                return vagaLivre;
            }
        }
        return "null";
    }

    public boolean liberaVaga(String vaga) {
        for (Vagas vagas : Vagas.values()) {
            if (vagas.name().equals(vaga)) {
                System.out.println("Vaga " + vagas + " liberada!");
                vagas.setStatus(0);
                return true;
            }
        }
        return false;
    }

    public int buscaCarro(String placa) {
        for (String key : veiculo.keySet()) {
            if (key.equals(placa)) {
                return veiculo.get(key);
            }
        }
        return 999;
    }

    public void mostrarVagas() {
        if (veiculo.size() == 0) {
            System.out.println("Estacionamento vazio");
        } else {
            System.out.println(carroVaga.entrySet());
        }
    }
}
