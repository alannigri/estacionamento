package com.company;

public class Main {

    public static void main(String[] args) {
        Estacionamento estacionamento = new Estacionamento();
        Carro carro;
        int opcao;
        do {
            opcao = IO.menu();
            if (opcao == 1) {
                carro = new Carro(IO.placa(),IO.CPF());
                estacionamento.entradaVeiculo(carro);
            } else if (opcao == 2) {
                    estacionamento.saidaVeiculo(IO.placa());
            }else if(opcao == 3){
                estacionamento.mostrarVagas();
            }
        } while (opcao != 4);
    }
}
