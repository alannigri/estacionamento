package com.company;

public enum Vagas {
    A0(0), A1(0), A2(0), A3(0), A4(0), A5(0), A6(0), A7(0), A8(0), A9(0),
    B0(0), B1(0), B2(0), B3(0), B4(0), B5(0), B6(0), B7(0), B8(0), B9(0),
    C0(0), C1(0), C2(0), C3(0), C4(0), C5(0), C6(0), C7(0), C8(0), C9(0),
    D0(0), D1(0), D2(0), D3(0), D4(0), D5(0), D6(0), D7(0), D8(0), D9(0),
    E0(0), E1(0), E2(0), E3(0), E4(0), E5(0), E6(0), E7(0), E8(0), E9(0);

    private int status;

    Vagas(int status){
        this.status=status;
    }

    public int getStatus(){
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}

